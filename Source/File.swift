//
//  File.swift
//  podThree
//
//  Created by Armin on 12/06/2020.
//

import Foundation
import Monri

@objc public class Armin : NSObject {
    
    private var creditCardNotValidMessage: String =  "credit_card_not_valid"
    private var tempCardId: String = ""
    private var cardProcessingIsDone: Bool = false
    private var cardProcessingIsDoneWithError: Bool = false
    private var cardProcessingErrorString: String = ""
    
    
    
    @objc public func printSome()-> String {
        
        let card =   Card(number: "5464000000000008",cvc: "286",expMonth: 12, expYear:2020)
        
        
        if !card.validateCard() {
            return "Card validation failed"
        } else {
            return "Card validation success"
        }
    }
    
    func setCardProcessingDoneWithError(value: Bool){
        cardProcessingIsDoneWithError = value
    }
    
    func setCardProcessingErrorString(value: String){
        cardProcessingErrorString = value
    }
    
    func setCardProcessingDone(value: Bool){
        cardProcessingIsDone = value
    }
    
    func setTempCardId(value: String){
        tempCardId = value
    }
    
    @objc public func getTempCardId() -> String{
        return tempCardId
    }
    
    @objc public func isCardProcessingDone() -> Bool{
        return cardProcessingIsDone
    }
    
    @objc public func isCardProcessingDoneWithError() -> Bool{
        return cardProcessingIsDoneWithError
    }
    
    @objc public func getCardProcessingErrorString() -> String{
        return cardProcessingErrorString
    }
    
    
    @objc public func getTempCardIdget(cardNumber:String , cardExpMonth:Int, cardExpYear:Int , cardCVC:String , tokenizeCard:Bool, merchantKey:String , authenticityToken:String )
    {
        let monri: MonriApi = {
            return MonriApi(authenticityToken: authenticityToken);
        }()
        
        let date = Date()
        let formatter = ISO8601DateFormatter()
        if #available(iOS 11.0, *) {
            formatter.formatOptions.insert(.withFractionalSeconds)
        } else {
            // Fallback on earlier versions
        }
        let timestamp = formatter.string(from: date);
        let token = UUID.init().uuidString
        // merchantKey, token, timestamp
        let digest = "\(merchantKey)\(token)\(timestamp)".sha512
        let tokenRequest = TokenRequest(token: token, digest: digest, timestamp: timestamp)
        
        var  card = Card( number:cardNumber, cvc:cardCVC, expMonth:cardExpMonth, expYear:cardExpYear)
        card.tokenizePan = tokenizeCard
        
        if !card.validateCard() {
            setCardProcessingDoneWithError(value: true)
            setCardProcessingErrorString(value:creditCardNotValidMessage)
            setCardProcessingDone(value:true)
        }
        else{
            monri.createToken(tokenRequest, paymentMethod: card) {
                result in
                switch result {
                case .error(let error):
                    self.setCardProcessingDoneWithError(value:true)
                    self.setCardProcessingErrorString(value:error.localizedDescription)
                    self.setCardProcessingDone(value:true)
                    
                case .token(let token):
                    self.setTempCardId(value:token.id)
                    self.setCardProcessingDone(value:true)
                }
            }
        }
    }
}
