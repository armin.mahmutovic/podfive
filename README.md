# podFive

[![CI Status](https://img.shields.io/travis/Armin/podFive.svg?style=flat)](https://travis-ci.org/Armin/podFive)
[![Version](https://img.shields.io/cocoapods/v/podFive.svg?style=flat)](https://cocoapods.org/pods/podFive)
[![License](https://img.shields.io/cocoapods/l/podFive.svg?style=flat)](https://cocoapods.org/pods/podFive)
[![Platform](https://img.shields.io/cocoapods/p/podFive.svg?style=flat)](https://cocoapods.org/pods/podFive)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

podFive is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'podFive'
```

## Author

Armin, armin.mahmutovic@ngs.ba

## License

podFive is available under the MIT license. See the LICENSE file for more info.
